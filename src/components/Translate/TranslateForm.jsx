import { useForm } from "react-hook-form";
import { SubmitButton } from "../reusables/Buttons";

const TranslateForm = ({ onTranslate }) => {

    const { register, handleSubmit, formState: { errors }} = useForm();

    const onSubmit = ({ phrase }) => { onTranslate(phrase) };

    return (
        <form className="form-translate-submit" onSubmit={ handleSubmit(onSubmit) }>
            <fieldset className="field-translate-input">
                <input className="input-translate" type="text" { ...register('phrase')} placeholder="Hello"/>
                <SubmitButton />
            </fieldset>
        </form>
    )
}
export default TranslateForm