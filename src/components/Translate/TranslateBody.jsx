import { addTranslationHistoryItem } from "../../api/translation";
import TranslateForm from "./TranslateForm";
import { SignLanguageIcon } from "../../components/reusables/SignLanguageIcon";
import { STORAGE_KEY_USER } from "../../const/storageKeys";
import { useUser } from "../../state/UserContext";
import { storageSave } from "../../utils/storage";
import { useState } from "react";

const TranslateBody = () => {
    const { user, setUser } = useUser();

    const [ translation, setTranslation ] = useState(null)

    const generateTranslationIcons = (phrase) => phrase.split("").map(
        char => <SignLanguageIcon char={char} />
    )    

    const handleTranslateClicked = async phrase => {
        if (!phrase) {
            alert('Please type in a phrase.');
            return
        }

        const [error, updatedUser] = await addTranslationHistoryItem(user, phrase);
        if (error !== null) {
            alert("Something went wrong...");
            return
        }

        //Synchronise UI and server state
        storageSave(STORAGE_KEY_USER, updatedUser);
        //Update context state
        setUser(updatedUser);
        setTranslation(phrase.toLowerCase())
    };

    return (
        <>
            <div className="el-container">
                <TranslateForm onTranslate={ handleTranslateClicked } />
                <div className="el-translate-output-container">
                    {translation && <section>
                        { generateTranslationIcons(translation) }
                    </section>}
                </div>
            </div>
        </>
    )
}
export default TranslateBody