import ProfileActions from "./ProfileActions"
import ProfileTranslationHistory from "./ProfileTranslationHistory"
import { useUser } from "../../state/UserContext"

//bundles the ProfileAction and ProfileTranslationHistory and the main content is then exported to the profile view
const ProfileMainContent = () =>
{
    const { user } = useUser()
    return (
        <div id="profile-main-content">
            <ProfileActions></ProfileActions>
            <ProfileTranslationHistory  translations = { user.translations } />
        </div>
    )
}
export default ProfileMainContent