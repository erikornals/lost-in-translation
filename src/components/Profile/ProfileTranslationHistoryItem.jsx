
//this exports the list elements to the ProfileHistoryElement so it can be mapped to an translation History array
const ProfileTranslationHistoryItem = ({translationItem}) => {
        return <li>{translationItem}</li>
}
export default ProfileTranslationHistoryItem