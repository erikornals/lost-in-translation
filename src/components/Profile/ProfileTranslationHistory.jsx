import ProfileTranslationHistoryItem from "./ProfileTranslationHistoryItem";
import { translationClearHistory } from "../../api/translation"
import { useUser } from "../../state/UserContext"
import { ClearTranslationHistoryButton } from "../reusables/Buttons";
import { storageSave } from "../../utils/storage";
import { STORAGE_KEY_USER } from "../../const/storageKeys";

//contains the the last 10 translations and the translation clear button
const ProfileTranslationHistory = ({translations}) =>
{
  const { user,setUser } = useUser()
  
  //ereases the  translation history
  const handleClearHistoryClick = async () => {
    if (!window.confirm('Are you sure you want to permanently clear your history?')){
        return
      }
    const [ clearError  ] = await translationClearHistory(user.id);

    if (clearError !== null){
        console.log("Unable to clear history.")
        return
      }
    
    const updatedUser = {
      ...user,
      translations: []
      }
    storageSave(STORAGE_KEY_USER, updatedUser)
    setUser(updatedUser)
  }

  const EntireTranslationList = translations.map((translationItem, index) => <ProfileTranslationHistoryItem key={index + '-' + translationItem} translationItem={translationItem} />);
  
  //keeps the last ten translations and is rendered to the page
  let lastTenTranslations = EntireTranslationList.slice(-10);
      return (
        <div id="translation-list-canvas">
            <h4>Your last 10 translations</h4>
            <ClearTranslationHistoryButton onClearHistoryClick={handleClearHistoryClick}></ClearTranslationHistoryButton>
             <ul>
                {lastTenTranslations}
             </ul> 
        </div>
    )
}
export default ProfileTranslationHistory
