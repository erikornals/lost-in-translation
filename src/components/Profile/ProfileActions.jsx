import { NavLink } from "react-router-dom"
import { storageDelete } from "../../utils/storage"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { useUser } from "../../state/UserContext"
import {LogoutButton, TranslatePageTurnerButton } from "../reusables/Buttons"

//the profileActions consists of a greeting, and most importantly two actions(buttons)
//on is for loggin out of the site, the second is to turn page towards translation page
const ProfileActions = () => {
    const { user, setUser } = useUser()

    const handleLogoutClick = () => {
        if (!window.confirm("do you really want to logout this awesome site?")){
            return
        }
        //deletes the current user from session storage,
        // and because of the state it logouts the current user
        storageDelete(STORAGE_KEY_USER)
            setUser(null)
    }

    return (
        <div id="section-canvas">
            <NavLink to="/translate"><TranslatePageTurnerButton></TranslatePageTurnerButton></NavLink>
            <h3 id="Greetings">hello, {user.username} !!! </h3>
            <LogoutButton onLogoutClick = {handleLogoutClick}></LogoutButton>
         </div> 
    )
}
export default ProfileActions