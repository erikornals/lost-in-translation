import LoginForm from "./LoginForm"

const LoginBody = () => {
    return (
        <>
            <div className="el-container">
                <div className="el-login-msg">Let's get started.</div>
                <LoginForm />
            </div>
        </>
    )
}
export default LoginBody