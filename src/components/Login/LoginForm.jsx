import { useState, useEffect } from 'react';
import { useForm } from 'react-hook-form'
import { loginUser } from '../../api/user'
import { storageSave } from '../../utils/storage';
import { useNavigate } from 'react-router-dom';
import { useUser } from '../../state/UserContext';
import { STORAGE_KEY_USER } from '../../const/storageKeys';
import { SubmitButton } from '../reusables/Buttons';

const usernameConfig = {
    required: true,
    minLength: 3
}

const LoginForm = () => {
    //Hooks
    const { register, handleSubmit, formState: { errors }} = useForm();
    const { user, setUser } = useUser();
    const navigate = useNavigate();

    //Local state
    const [ loading, setLoading ] = useState(false);
    const [ apiError, setApiError ] = useState(null);

    //Side effects
    useEffect(() => {
        if (user !== null){
            navigate('/translate');
        }
    }, [ user, navigate ])

    //Event handlers
    const onSubmit = async ({ username }) => {
        setLoading(true);
        const [error, currentUser] = await loginUser(username);
        if (error !== null) {
            setApiError(error);
        }
        if (currentUser !== null){
            storageSave(STORAGE_KEY_USER, currentUser);
            setUser(currentUser);
        }
        setLoading(false);
    };

    //Render functions
    const errorMessage = (() => {
        if (!errors.username) {
            return null
        }

        if (errors.username.type === 'required'){
            return <span className='el-login-help-msg'> Username is required.</span>
        }

        if (errors.username.type === 'minLength'){
            return <span className='el-login-help-msg'> Username is too short (minimum 3 characters).</span>
        }
    })()

    return (
        <>
            <form className="form-login-submit" onSubmit = { handleSubmit(onSubmit) }>
                <fieldset className="field-login-input">
                    <label htmlFor="username"></label>
                    <img className='icon-login-profile' src="icon_profile.svg" alt="logo" />
                    <input
                        className='input-login'
                        type="text" 
                        placeholder="What's your name?"
                        { ...register("username", usernameConfig) } 
                    />
                    <SubmitButton
                        disabled={ loading }/>
                </fieldset>
                { errorMessage }
                {loading && <p className='el-login-help-msg'>Logging in...</p>}
                { apiError && <p>{ apiError }</p>}
            </form>
        </>
    )
}
export default LoginForm