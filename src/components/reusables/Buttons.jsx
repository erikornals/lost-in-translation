import { useState } from "react"
import { NavLink } from "react-router-dom"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { storageRead } from "../../utils/storage"


//this button changes page to profile from translate and shows the current user in the profile page
export const ProfileButton = () => {
    const username = storageRead(STORAGE_KEY_USER).username

    return (
        <NavLink to="/profile">
            <button className="btn-profile">
                <img className="icon-translate-profile" src="icon_profile_white.svg" alt="icon_profile" />
                <p className="el-profile-name">{ username }</p>
            </button>
        </NavLink>
    )
}

export const LogoutButton = ({onLogoutClick}) => 
{
    return (
            <button className="oval-button" id="btn-logout" onClick={onLogoutClick}>Log out</button>
    )
}

export const ClearTranslationHistoryButton = ({onClearHistoryClick}) =>
{
    return (
        <button className="oval-button" id="clear-translation-history-button" onClick={onClearHistoryClick}>Clear Translations</button>
    )
}

export const TranslatePageTurnerButton = () =>
{
    return (
        <button className="oval-button" id="btn-translation-page-button" >Translation Page</button>
    )
}


export const SubmitButton = () => {
    const okIcon = <img className="icon-btn-submit" src="ok_icon.svg" alt="ok"/>
    const okIconHover = <img className="icon-btn-submit" src="ok_icon_hover.svg" alt="ok-hover" />

    const [currentIcon, setCurrentIcon] = useState(okIcon);

    return (
        <button
            className="btn-submit"
            type="submit"
            onMouseEnter={() => setCurrentIcon(okIconHover)}
            onMouseLeave={() => setCurrentIcon(okIcon)}
        >
            { currentIcon }
        </button>
    )
}
