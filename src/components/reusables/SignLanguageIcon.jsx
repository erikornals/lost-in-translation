export const SignLanguageIcon = ({ char }) => {
    if (char === " "){
        return (
            <img src='slash.svg' alt="space" width="" />
        )
    } else if (char.match(/[a-z]/i)){
        return (
            <img className="icon-sign-lang" src={ `img/${char}.svg` } alt={ char } width="" />
        )
    }
}