import "../../stylesheets/reusables.css"

const Footer = () => {
    return (
        <>
            <div className="el-footer">
                <p className="footer-credit">Sign language icons created by David S from the Noun Project</p>
            </div>
        </>
    )
}
export default Footer