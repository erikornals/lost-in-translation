import "../../stylesheets/reusables.css"

const Header = ( { button }) => {
    return (
        <>
            <div className="el-header">
                <img className="logo" src="logo.svg" alt="logo" />
                { button }
            </div>
        </>
    )
}
export default Header