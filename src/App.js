import './App.css';
import './stylesheets/reusables.css'
import './stylesheets/Login.css'
import './stylesheets/Translate.css'
import './stylesheets/Profile.css'
import {
  BrowserRouter,
  Routes,
  Route
} from 'react-router-dom';
import Login from './views/Login';
import Translate from './views/Translate';
import Profile from './views/Profile';
import "@fontsource/roboto-slab/100.css";
import "@fontsource/roboto-slab/400.css";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Routes>
          <Route path="/" element={<Login />} />
          <Route path="/translate" element={<Translate />} />
          <Route path="/profile" element={<Profile />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}
export default App;