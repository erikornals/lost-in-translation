import Header from "../components/reusables/Header"
import withAuth from "../hoc/withAuth"
import ProfileMainContent from "../components/Profile/ProfileMainContent"
import { ProfileButton } from "../components/reusables/Buttons";

//renders the page to the profile page
const Profile = () => {

    return(
        <>
            <Header button = {<ProfileButton/>} />
            <ProfileMainContent></ProfileMainContent>
        </>
    )
}
export default withAuth(Profile)