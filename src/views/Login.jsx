import LoginBody from "../components/Login/LoginBody"
import Footer from "../components/reusables/Footer"
import Header from "../components/reusables/Header"

const Login = () => {
    return(
        <>
            <Header />
            <LoginBody />
            <Footer />
        </>
    )
}
export default Login