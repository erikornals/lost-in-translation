import withAuth from "../hoc/withAuth"
import { ProfileButton } from "../components/reusables/Buttons";
import Header from "../components/reusables/Header";
import Footer from "../components/reusables/Footer";
import TranslateBody from "../components/Translate/TranslateBody";

const Translate = () => {

    return(
        <>
            <Header button = {<ProfileButton/>}/>
            <TranslateBody />
            <Footer />
        </>
    )
}
export default withAuth(Translate)